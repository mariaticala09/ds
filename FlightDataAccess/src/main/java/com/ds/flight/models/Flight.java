package com.ds.flight.models;
import java.io.Serializable;
import java.sql.Timestamp;

public class Flight implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idFlight;
	private Integer flightNr;
	private String airplaneType;
	private String departureCity;
	private Timestamp departureDH;
	private String arrivalCity;
	private Timestamp arrivalDH;

	public Integer getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(Integer idFlight) {
		this.idFlight = idFlight;
	}

	public Integer getFlightNr() {
		return flightNr;
	}

	public void setFlightNr(Integer flightNr) {
		this.flightNr = flightNr;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public String getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}

	public Timestamp getDepartureDH() {
		return departureDH;
	}

	public void setDepartureDH(Timestamp departureDH) {
		this.departureDH = departureDH;
	}

	public String getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public Timestamp getArrivalDH() {
		return arrivalDH;
	}

	public void setArrivalDH(Timestamp arrivalDH) {
		this.arrivalDH = arrivalDH;
	}

}
